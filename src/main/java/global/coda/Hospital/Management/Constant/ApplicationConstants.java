package global.coda.Hospital.Management.Constant;

public class ApplicationConstants {
	public static final String MESSAGES_BUNDLE = "messages";
	public static final String NUMERIC_REGEX = "[+-]?[0-9][0-9]*";
	public static final String EMPTY_STRING="";
	public static final String PATH="E:\\ecllipse-w\\hospitalman-master\\src\\main\\java\\csvfile\\patientDatabase.csv";
	public static final String Temp_PATH="E:\\ecllipse-w\\hospitalman-master\\src\\main\\java\\csvfile\\";
	public static final String TEMP_FILES="tempfiles";
	public static final String CSV=".csv";
	
	//Log Codes
	public static final String HM100I = "HM100I";
	public static final String HM101I = "HM101I";
	public static final String HM102I = "HM102I";
	public static final String HM103I = "HM103I";
	public static final String HM104I = "HM104I";
	public static final String HM105I = "HM105I";
	public static final String HM106I = "HM106I";
	public static final String HM107I = "HM107I";
	public static final String HM108I = "HM108I";
	public static final String HM109I = "HM109I";
	public static final String HM110I = "HM110I";
	public static final String HM111I = "HM111I";
	public static final String HM112I = "HM112I";
	public static final String HM113I = "HM113I";
	public static final String HM114I = "HM114I";
	public static final String HM115I = "HM115I";
	public static final String HM116I = "HM116I";
	public static final String HM117I = "HM117I";
	public static final String HM118I = "HM118I";
	public static final String HM119I = "HM119I";
	public static final String HM120I = "HM120I";
	public static final String HM121I = "HM121I";
	
	public static final String HM200E = "HM200E";
	public static final String HM201E = "HM201E";
	public static final String HM202E = "HM202E";
	public static final String HM203E = "HM203E";
	public static final String HM204E = "HM204E";
	public static final String HM205E = "HM205E";
	public static final String HM206E = "HM206E";
	public static final String HM207E = "HM207E";
	public static final String HM208E = "HM208E";
	public static final String HM209E = "HM209E";
	public static final String HM210E = "HM210E";

	public static final String HM301D = "HM301D";
	public static final String HM302D = "HM302D";
	public static final String HM303D = "HM303D";
	public static final String HM304D = "HM304D";
	public static final String HM305D = "HM305D";
	public static final String HM306D = "HM306D";
	
}
