package global.coda.Hospital.Management.model;

public class Patient {
private int patientId;
private String patientName;
private byte patientAge;
private long patientPhoneNo;
private String patientDisease;

public Patient() {
	
}

public Patient(int PatientID,String PatientName,byte PatientAge,long PatientPhoneNO,String PatientDisease) {
	this.patientId=PatientID;
	this.patientName=PatientName;
	this.patientAge=PatientAge;
	this.patientPhoneNo=PatientPhoneNO;
	this.patientDisease=PatientDisease;
}



public int getPatientId() {
	return patientId;
}



public void setPatientId(int patientId) {
	this.patientId = patientId;
}



public String getPatientName() {
	return patientName;
}



public void setPatientName(String patientName) {
	this.patientName = patientName;
}



public byte getPatientAge() {
	return patientAge;
}



public void setPatientAge(byte patientAge) {
	this.patientAge = patientAge;
}



public long getPatientPhoneNo() {
	return patientPhoneNo;
}



public void setPatientPhoneNo(long patientPhoneNo) {
	this.patientPhoneNo = patientPhoneNo;
}



public String getPatientDisease() {
	return patientDisease;
}



public void setPatientDisease(String patientDisease) {
	this.patientDisease = patientDisease;
}



@Override
public String toString() {
	return   "Patient[Patient ID "+patientId+",Patient Name "+patientName+",Patient Age "+patientAge+",Patient Phoneno "+patientPhoneNo+",patient disease "+patientDisease;
}

public String toCsvString() {
	return   patientId+","+patientName+","+patientAge+","+patientPhoneNo+","+patientDisease;
}
}
