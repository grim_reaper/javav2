package global.coda.Hospital.Management.PatientServices;
import static global.coda.Hospital.Management.Constant.ApplicationConstants.*;

import global.coda.Hospital.Management.Constant.ApplicationConstants;
//import global.coda.Hospital.Management.Main.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import global.coda.Hospital.Management.Main.ApplicationMain;
import global.coda.Hospital.Management.model.Patient;

public class PatientService implements InterfaceOperation{
	
	private static final Logger logger=LoggerFactory.getLogger(PatientService.class);
	private static final ResourceBundle LOCAL_MESSAGES = ResourceBundle.getBundle(ApplicationConstants.MESSAGES_BUNDLE);
	private String patientName=EMPTY_STRING;
	private byte patientAge=0;
	private int patientId=0;
	private long patientPhoneNo=0;
	private String patientDisease=EMPTY_STRING;
	String line=EMPTY_STRING;
	Scanner sc=new Scanner(System.in);
    private Map<Integer,Patient> patientdata=new HashMap<Integer,Patient>();
    Map<Integer,Object> ReadData=new HashMap<Integer,Object>();
	@Override
	public Patient createPatient(int PatientID, String PatientName, byte PatientAge, long PatientPhoneNO,
			String PatientDisease) {
		Patient patient=new Patient(PatientID, PatientName, PatientAge, PatientPhoneNO, PatientDisease);
		patientdata.put(PatientID,patient);
		//noOfPatients++
		try {File fileName=new File(PATH);
			 //FileWriter Access=new FileWriter(PATH,true);
			  logger.debug(LOCAL_MESSAGES.getString(HM121I));
			  if(!fileName.exists()) {
				  File Tempfile=File.createTempFile(TEMP_FILES,CSV);
				  fileName.renameTo(Tempfile);
			  }
				  if(patientdata.size()==1)
			        {
			      	  BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));
		  	            String stringToWrite = patient.toCsvString();
		  	            bufferedWriter.write(stringToWrite);
		  	            bufferedWriter.newLine();
		  	            bufferedWriter.flush();
		  	            bufferedWriter.close();
			        }
			        else {
			        for(int i:patientdata.keySet()) {
			        	if(i==PatientID)
			        	{
			        		  BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));
			  	            String stringToWrite = patient.toCsvString();
			  	            bufferedWriter.write(stringToWrite);
			  	            bufferedWriter.newLine();
			  	            bufferedWriter.flush();
			  	            bufferedWriter.close();
			  	            //fileName.close(); 
			        	}
			        	
			  }
			          }  
		}
		catch(Exception e){
			logger.error("file not found");
		}
		return null;
	}
	@Override
	public Patient removePatient(int PatientId) {
		Iterator<Integer> itr=patientdata.keySet().iterator();
		while(itr.hasNext())
		{   Integer Key=itr.next();
			if(patientdata.get(Key).getPatientId()==PatientId) {
				patientdata.remove(PatientId);
			}
		}	
		return null;
	}
	
	
	@Override
	public boolean updatePatient(int patientID, String patientName, byte patientAge, long patientPhoneNO,
			String patientDisease) {
		Patient patient=new Patient(patientID, patientName, patientAge, patientPhoneNO, patientDisease);
		patientdata.put(patientID,patient);
		 try {
			    FileWriter Access=new FileWriter(PATH,true);
		        logger.debug("data been feeded");
	            BufferedWriter bufferedWriter = new BufferedWriter(Access);
	            String stringToWrite = patient.toCsvString();
	            bufferedWriter.write(stringToWrite);
	            bufferedWriter.newLine();
	            bufferedWriter.flush();
	            Access.close();
	            bufferedWriter.close();
		 }
		 catch(Exception e){
			    	e.printStackTrace();
			    }
		return false;
	}
	@Override
	public Patient readPatient(int Patientid) {
		Patient patient = patientdata.get(Patientid);
		logger.debug(patient.toString());
	     
		 try {
			    FileReader Access=new FileReader(PATH);
		        logger.debug("data been feeded");
		        //Patient readobj=new Patient(patientId, patientName, patientAge, patientPhoneNo, patientDisease);
	            BufferedReader bufferReader = new BufferedReader(Access);
	            while((line=bufferReader.readLine())!=null) {
	             String[] patientDataRead=line.split(",");
	             patientId=Integer.parseInt(patientDataRead[0]);
	             patientName=patientDataRead[1];
	             patientAge=Byte.parseByte(patientDataRead[2]);
	             patientPhoneNo=Long.parseLong(patientDataRead[3]);
	             patientDisease=patientDataRead[4];
	             Patient readobj=new Patient(patientId, patientName, patientAge, patientPhoneNo, patientDisease);
	             ReadData.put(patientId,readobj);
	            }
	            logger.debug(ReadData.toString());
	            Access.close();
	            bufferReader.close();   
		 }
		 catch(Exception e){
			    	e.printStackTrace();
			    }
		return patient;
	}
	@Override
	public Patient sendPatient(int patientid) {
		logger.debug("data achieved");
		Patient patient=patientdata.get(patientid);
		logger.debug("patient"+toString());
	    return patient;
	}
	
	@Override
	public int patientId() {
		int patientId=0;
		try {
			patientId = sc.nextInt();
			if(patientId==0)
			{
				logger.error(LOCAL_MESSAGES.getString(HM208E));
				logger.debug(LOCAL_MESSAGES.getString(HM302D));
				patientId();
			}
		} catch (InputMismatchException e) {
			logger.error(LOCAL_MESSAGES.getString(HM201E));
			logger.debug(LOCAL_MESSAGES.getString(HM302D));
			patientId();
		}
		return patientId;
	}
	
	@Override
	public int patientChoice() {
		  int patientChoice=0;
			try {
				 patientChoice = sc.nextInt();
				 if(patientChoice<0 && patientChoice>4) {
					 logger.error(LOCAL_MESSAGES.getString(HM200E));
						logger.debug(LOCAL_MESSAGES.getString(HM301D));
						patientChoice();
				 }
			} catch (InputMismatchException e) {
				logger.error(LOCAL_MESSAGES.getString(HM200E));
				logger.debug(LOCAL_MESSAGES.getString(HM301D));
				patientChoice();
			}
			return patientChoice;
		}
	
	@Override
	public byte patientAge() {
		byte patientAge=0;
		try {
			patientAge = sc.nextByte();
			if (patientAge > 128 || patientAge < 0) {
				logger.error(LOCAL_MESSAGES.getString(HM204E));
				logger.debug(LOCAL_MESSAGES.getString(HM304D));
				patientAge();
			}
		} catch (InputMismatchException e) {
			logger.error(LOCAL_MESSAGES.getString(HM203E));
			logger.debug(LOCAL_MESSAGES.getString(HM304D));
			patientAge();
		}
		return patientAge;
	}
	
	@Override
	public String patientDisease() {
		String patientDisease=EMPTY_STRING;
		try {
			sc.nextLine();
			patientDisease = sc.nextLine();
			if (patientDisease == EMPTY_STRING) {
				logger.error(LOCAL_MESSAGES.getString(HM210E));
				logger.debug(LOCAL_MESSAGES.getString(HM306D));
				patientDisease();
			}
			
		} catch (InputMismatchException e) {
	    	 logger.error(LOCAL_MESSAGES.getString(HM206E));
	    	 logger.debug(LOCAL_MESSAGES.getString(HM306D));
				patientDisease();
		}
		return patientDisease;
	}
	
	@Override
	public long patientPhoneNo() {
		long patientPhoneNo=0;
		try {
			patientPhoneNo = sc.nextLong();
			if (!(String.valueOf(patientPhoneNo).length() == 10)) {
				logger.error(LOCAL_MESSAGES.getString(HM207E));
				logger.debug(LOCAL_MESSAGES.getString(HM305D));
				patientPhoneNo();
			}
		} catch (InputMismatchException e) {
			logger.error(LOCAL_MESSAGES.getString(HM205E));
			logger.debug(LOCAL_MESSAGES.getString(HM305D));
			patientPhoneNo();
		}
		return patientPhoneNo;
	}
	
	@Override
	public String patientName() {
		String patientName= EMPTY_STRING;
		try {
			sc.nextLine();
			patientName = sc.nextLine();
			if (patientName == EMPTY_STRING) {
				logger.error(LOCAL_MESSAGES.getString(HM209E));
				logger.debug(LOCAL_MESSAGES.getString(HM303D));
				patientName();
			}
		} catch (InputMismatchException e) {
			logger.error(LOCAL_MESSAGES.getString(HM202E));
			logger.debug(LOCAL_MESSAGES.getString(HM303D));
			patientName();
		}
		return patientName;
	}
}
