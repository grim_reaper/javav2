package global.coda.Hospital.Management.PatientServices;
import global.coda.Hospital.Management.model.Patient;

public interface InterfaceOperation {
public Patient createPatient(int PatientID,String PatientName,byte PatientAge,long PatientPhoneNO,String PatientDisease);
public Patient removePatient(int id);
public boolean updatePatient(int patientID,String patientName,byte patientAge,long patientPhoneNO,String patientDisease);
public Patient readPatient(int id);
public Patient sendPatient(int id);
public int patientId();
public int patientChoice();
public byte patientAge();
public String patientDisease();
public long patientPhoneNo();
public String patientName();
}
