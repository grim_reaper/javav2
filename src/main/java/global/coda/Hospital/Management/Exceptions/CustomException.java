package global.coda.Hospital.Management.Exceptions;

import java.util.ResourceBundle;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

//import ch.qos.logback.classic.Logger;
import global.coda.Hospital.Management.Constant.ApplicationConstants;
import global.coda.Hospital.Management.Main.ApplicationMain;

public class CustomException extends Exception{
	 private static final Logger logger=LoggerFactory.getLogger(CustomException.class);
	 private static final ResourceBundle LOCAL_MESSAGES = ResourceBundle.getBundle(ApplicationConstants.MESSAGES_BUNDLE);
	public CustomException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CustomException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CustomException(String message) {
		super(message);
	    logger.error(message);
	}

	public CustomException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
