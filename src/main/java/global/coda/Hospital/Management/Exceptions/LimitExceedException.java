package global.coda.Hospital.Management.Exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import global.coda.Hospital.Management.Main.ApplicationMain;

public class LimitExceedException extends Exception{
	 private static final Logger logger=LoggerFactory.getLogger(LimitExceedException.class);
	public LimitExceedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LimitExceedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public LimitExceedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LimitExceedException(String message) {
		super(message);
	}

	public LimitExceedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
