package global.coda.Hospital.Management.Exceptions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import global.coda.Hospital.Management.Constant.ApplicationConstants;
import global.coda.Hospital.Management.Main.ApplicationMain;

public class InputInvalidException extends Exception{
	 private static final Logger logger=LoggerFactory.getLogger(ApplicationMain.class);
	// private static final ResourceBundle LOCAL_MESSAGES = ResourceBundle.getBundle(ApplicationConstants.MESSAGES_BUNDLE);
	public InputInvalidException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InputInvalidException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InputInvalidException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InputInvalidException(String message) {
		//super(message);
	    logger.error(message);
	}

	public InputInvalidException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
