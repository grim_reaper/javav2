package global.coda.Hospital.Management.Main;

import static global.coda.Hospital.Management.Constant.ApplicationConstants.*;
//import java.util.InputMismatchException;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import global.coda.Hospital.Management.Constant.ApplicationConstants;
import global.coda.Hospital.Management.Exceptions.CustomException;
import global.coda.Hospital.Management.Exceptions.InputInvalidException;
import global.coda.Hospital.Management.Exceptions.LimitExceedException;
import global.coda.Hospital.Management.PatientServices.PatientService;
import global.coda.Hospital.Management.model.Patient;
/**
 * 
 * @author vishnu.c
 *
 */

public class ApplicationMain {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationMain.class);
	private static final ResourceBundle LOCAL_MESSAGES = ResourceBundle.getBundle(ApplicationConstants.MESSAGES_BUNDLE);
	
	public static void main(String[] args) throws InputInvalidException, LimitExceedException, CustomException {
		
		//object to access services
		PatientService patientService = new PatientService();
		
		/**
		 * This is an Application based on "Hospital Management System"
		 * where the patient details like his id,age,phone no and disease 
		 * will be fetched and stored in a Excel sheet
		 * 
		 * 
		 * operations like Add patient, Remove patient, Read patient, Update patient
		 * 
		 */
		
		//List<Patient> patientList = new ArrayList<Patient>();
		
		//Dummy data been used to avoid exceptions
		int patientChoice = 0;
		int patientId = 0;
		String patientName = EMPTY_STRING;
		byte patientAge = 0;
		long patientPhoneNo = 0;
		String patientDisease = EMPTY_STRING;
		boolean isTrue = true;
		
		//choice of patient 
		while (isTrue) {
			logger.info(LOCAL_MESSAGES.getString(HM100I));
			logger.info(LOCAL_MESSAGES.getString(HM101I));
			logger.info(LOCAL_MESSAGES.getString(HM102I));
			logger.info(LOCAL_MESSAGES.getString(HM103I));
			logger.info(LOCAL_MESSAGES.getString(HM104I));
			
			//from patientChoice
			patientChoice=patientService.patientChoice();
			switch (patientChoice) {
			case 0:
				isTrue = false;
				logger.debug(LOCAL_MESSAGES.getString(HM108I));
				break;
			case 1:
				logger.info(LOCAL_MESSAGES.getString(HM105I));
				logger.info(LOCAL_MESSAGES.getString(HM106I));
				patientId=patientService.patientId();
				patientName=patientService.patientName();
				patientAge=patientService.patientAge();
				patientPhoneNo=patientService.patientPhoneNo();
				patientDisease=patientService.patientDisease();
				patientService.createPatient(patientId, patientName, patientAge, patientPhoneNo, patientDisease);
				//Patient patientData = new Patient(patientID, patientName, patientAge, patientPhoneNo, patientDisease);
				//patientList.add(patientData);
				break;
			case 2:
				logger.info(LOCAL_MESSAGES.getString(HM109I));
				patientId=patientService.patientId();
				patientService.removePatient(patientId);
				break;
			case 3:
				logger.info(LOCAL_MESSAGES.getString(HM109I));
				patientId=patientService.patientId();
				patientService.readPatient(patientId);
				break;
			case 4:
				logger.info(LOCAL_MESSAGES.getString(HM111I));
				patientId=patientService.patientId();
				Patient patient = patientService.sendPatient(patientId);
				patientName = patient.getPatientName();
				patientAge = patient.getPatientAge();
				patientPhoneNo = patient.getPatientPhoneNo();
				patientDisease = patient.getPatientDisease();
				logger.info(LOCAL_MESSAGES.getString(HM112I));
				logger.info(LOCAL_MESSAGES.getString(HM113I));
				logger.info(LOCAL_MESSAGES.getString(HM114I));
				logger.info(LOCAL_MESSAGES.getString(HM115I));
				logger.info(LOCAL_MESSAGES.getString(HM116I));
				patientChoice=patientService.patientChoice();
				switch (patientChoice) {
				case 0:
					logger.info(LOCAL_MESSAGES.getString(HM117I));
					patientName = patientService.patientName();
					break;
				case 1:
					logger.info(LOCAL_MESSAGES.getString(HM118I));
					patientAge = patientService.patientAge();
					break;
				case 2:
					logger.info(LOCAL_MESSAGES.getString(HM119I));
					patientPhoneNo = patientService.patientPhoneNo();
					break;
				case 3:
					logger.info(LOCAL_MESSAGES.getString(HM120I));
					patientDisease = patientService.patientDisease();
				default:
					logger.error(LOCAL_MESSAGES.getString(HM200E));
				}
				patientService.updatePatient(patientId, patientName, patientAge, patientPhoneNo, patientDisease);
			default:
				logger.error(LOCAL_MESSAGES.getString(HM200E));
				isTrue = false;
			}
		}
	}
	
}
